const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const schema = Schema({
  _name:String,
  _lastName:String
});

class Actor {

  constructor(name, lastName){
    this._name = name;
    this._lastName = lastName;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }

  get lastName(){
    return this._lastName;
  }

  set lastName(v){
    this._lastName = v;
  }
}

schema.plugin(mongoosePaginate);
schema.loadClass(Actor);
module.exports = mongoose.model('Actor', schema);
