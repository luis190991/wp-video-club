const express = require('express');
const Actor = require('../../models/actor');

function list(req, res, next){
  //Actor.find({}).then(actors => res.json(actors));
  let page = req.params.page ? req.params.page : 1;
  const options = {
    page: page,
    limit: 2
  };

  Actor.paginate({}, options)
  .then(actors => {
    res.json({
      message: res.__('ok'),
      error: false,
      objs: actors
    });
  })
  .catch(()=>{

  });
}

function index(req, res, next){
  let id = req.params.id;
  console.log(`El id de usuario es ${id}`);
  let user = {};
  res.json(user);
}

function create(req, res, next){
  let name = req.body.name;
  let lastName = req.body.lastName;
  let actor = new Actor({_name:name, _lastName:lastName});

  actor.save()
  .then((obj)=>{
    res.json(obj);
  })
  .catch((err)=>{
    res.status(500).json({});
  });
}

function update(req, res, next){
  res.render('index', {title:'se actualizo un elemento'});
}

function destroy(req, res, next){
  res.render('index', {title:'elimino un elemento'});
}

module.exports = {
  index, list, create, update, destroy
}
